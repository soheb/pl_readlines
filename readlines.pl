#!/usr/bin/perl

use strict;
use warnings;

my $file = 'test.txt';

if ((scalar(@ARGV) == 1)) {
	$file = $ARGV[0];
}

open my $info, $file or die "Could not open $file: $!";

my $count = 0;
while(my $line = <$info>) {
	$count++;
}

close $info;

print "There are $count lines in the file $file\n"